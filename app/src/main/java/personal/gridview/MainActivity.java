package personal.gridview;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {
    private EditText editTextTitle;
    private EditText editTextDetails;
    private Button buttonAdd;
    private GridView gridView;
    private ArrayList<TitleDetails> titlesDetails = new ArrayList<TitleDetails>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.init();
    }

    private void init() {
        this.editTextTitle = (EditText) this.findViewById(R.id.main_input_edit_text_title);
        this.editTextDetails = (EditText) this.findViewById(R.id.main_input_edit_text_details);
        this.buttonAdd = (Button) this.findViewById(R.id.main_input_button_add);
        this.gridView = (GridView) this.findViewById(R.id.main_grid_view);

        this.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = MainActivity.this.editTextTitle.getText().toString();
                String details = MainActivity.this.editTextDetails.getText().toString();

                if( (title.equals("")) || (title==null) ) {
                    return;
                }
                MainActivity.this.editTextTitle.setText("");
                MainActivity.this.editTextDetails.setText("");
                details = (details==null) ? "" : details;
                TitleDetails titleDetails = new TitleDetails(title,details);
                MainActivity.this.titlesDetails.add(titleDetails);
                MainActivity.this.gridView.setAdapter(new CellAdapter(MainActivity.this,MainActivity.this.titlesDetails));
            }
        });
    }

}
