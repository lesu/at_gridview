package personal.gridview;


final class TitleDetails {
    String title;
    String details;

    TitleDetails(String title, String details) {
        this.title = title;
        this.details = details;
    }
}
